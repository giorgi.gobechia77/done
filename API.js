import { SERVER_ADDR} from "./Config.js";

const call = async (url) => {
    const req = await fetch(SERVER_ADDR + url);
    const result = await req.json();
    return result
}

export const product = async (sort = null) => {
    return await call(`products${sort ? `?sort=${sort}` : ''}`);
};

export const categories = async () => await call(`product/categories`)