import { product } from './API.js';
import { generateProductHTML } from "./Product.js";

const catalog = document.getElementsByClassName('catalog')[0]

const fillUpCatalog = async (sort = null) => {
    const productList = await product(sort)
    catalog.innerHTML = generateProductHTML(productList)
}
fillUpCatalog();
